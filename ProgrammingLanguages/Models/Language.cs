using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ProgrammingLanguages.Models;

public class Language
{
    [Key]
    public int Id { get; set; }
    
    [Required(ErrorMessage = "Required field")]
    [DisplayName("Name")]
    public string? Name { get; set; }
    
    [Required(ErrorMessage = "Required field")]
    [DisplayName("Year")]
    public int Year { get; set; }
    
    [Required(ErrorMessage = "Required field")]
    [DisplayName("Family")]
    public string? Family { get; set; }
    
    [Required(ErrorMessage = "Required field")]
    [DisplayName("Typing discipline")]
    public string? TypingDiscipline { get; set; }
    
    [Required(ErrorMessage = "Required field")]
    [DisplayName("Extensions")]
    public string? Extensions { get; set; }
    
}