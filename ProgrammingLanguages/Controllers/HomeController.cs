﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProgrammingLanguages.Data;
using ProgrammingLanguages.Models;

namespace ProgrammingLanguages.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ProgrammingLanguagesContext _dbContext;

    public HomeController(ILogger<HomeController> logger, ProgrammingLanguagesContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }


    public async Task<IActionResult> Index()
    {
        return View(await _dbContext.Language.ToListAsync());
    }

    public IActionResult Create()
    {
        return View(new Language());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(Language lang)
    {
        if (ModelState.IsValid)
        {
            _dbContext.Add(lang);
            await _dbContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        return View(lang);
    }

    public IActionResult Update(int id)
    {
        return View(_dbContext.Language.Find(id));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Update(
        [Bind("Id, Name, Year, Family, TypingDiscipline, Extensions")] Language lang)
    {
        if (ModelState.IsValid)
        {
            _dbContext.Update(lang);
            await _dbContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        return View(lang);
    }

    public async Task<IActionResult> Delete(int? id)
    {
        var lang = await _dbContext.Language.FindAsync(id);
        _dbContext.Language.Remove(lang);
        await _dbContext.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }
    
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}