using Microsoft.EntityFrameworkCore;
using ProgrammingLanguages.Models;

namespace ProgrammingLanguages.Data;

public class ProgrammingLanguagesContext : DbContext
{
    public DbSet<Language> Language => Set<Language>();

    public ProgrammingLanguagesContext(DbContextOptions options) : base(options) {}
    
}