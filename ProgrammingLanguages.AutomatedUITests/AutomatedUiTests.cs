using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Xunit;

namespace ProgrammingLanguages.AutomatedUITests;

public class AutomatedUiTests : IDisposable
{
    private readonly IWebDriver _webDriver;
    private readonly HomePage _page;

    public AutomatedUiTests()
    {
        _webDriver = new ChromeDriver();
        _page = new HomePage(_webDriver);
        _page.Navigate();
    }
    


    [Fact]
    public void ReturnCreateView()
    {
        Assert.Equal("Create - ProgrammingLanguages", _page.Title);
        Assert.Contains("Typing discipline", _page.Source);
    }

    [Fact]
    public void EmptyRequiredField()
    {
        _page.SendName("Test");
        _page.ClickCreate();
        
        Assert.Equal("Required field", _page.YearErrorMessage);
    }

    [Fact]
    public void SuccessfulCreation()
    {
        _page.SendName("Go");
        _page.SendYear("2009");
        _page.SendFamily("C");
        _page.SendTypingDiscipline("Inferred, static, strong, structural, nominal");
        _page.SendExtensions(".go");
        _page.ClickCreate();
        
        Assert.Equal("Programming Languages - ProgrammingLanguages", _page.Title);
        Assert.Contains("Go", _page.Source);
        Assert.Contains("2009", _page.Source);
        Assert.Contains("C", _page.Source);
        Assert.Contains("Inferred, static, strong, structural, nominal", _page.Source);
        Assert.Contains(".go", _page.Source);
    }
    
    public void Dispose()
    {
        _webDriver.Quit();
        _webDriver.Dispose();
    }
}