using OpenQA.Selenium;

namespace ProgrammingLanguages.AutomatedUITests;

public class HomePage
{ 
    private readonly IWebDriver _webDriver;
    private const string uri = "https://localhost:7263/Home/Create";
    private IWebElement NameElement => _webDriver.FindElement(By.Id("Name"));
    private IWebElement YearElement => _webDriver.FindElement(By.Id("Year"));
    private IWebElement FamilyElement => _webDriver.FindElement(By.Id("Family"));
    private IWebElement TypingDisciplineElement => _webDriver.FindElement(By.Id("TypingDiscipline"));
    private IWebElement ExtensionsElement => _webDriver.FindElement(By.Id("Extensions"));
    private IWebElement CreateElement => _webDriver.FindElement(By.Id("create"));

    public string Title => _webDriver.Title;
    public string Source => _webDriver.PageSource;
    public string YearErrorMessage => _webDriver.FindElement(By.Id("Year-error")).Text;

    public HomePage(IWebDriver webdriver)
    {
        _webDriver = webdriver;
    }

    public void Navigate() => _webDriver.Navigate().GoToUrl(uri);

    public void SendName(string name) => NameElement.SendKeys(name);
    public void SendYear(string year) => YearElement.SendKeys(year);
    public void SendFamily(string family) => FamilyElement.SendKeys(family);
    public void SendTypingDiscipline(string td) => TypingDisciplineElement.SendKeys(td);
    public void SendExtensions(string ext) => ExtensionsElement.SendKeys(ext);
    public void ClickCreate() => CreateElement.Click();
}